package com.example.tristanfreeman.winehound;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by tristanfreeman on 4/3/15.
 */
public class WineDataSource {

    private SQLiteDatabase database;
    private wineSQLiteHelper dbHelper;
    private String[] allColumns = {
            wineSQLiteHelper.COLUMN_ID,
            wineSQLiteHelper.COLUMN_NAME,
            wineSQLiteHelper.COLUMN_BRAND,
            wineSQLiteHelper.COLUMN_RETAILER,
            wineSQLiteHelper.COLUMN_ML,
            wineSQLiteHelper.COLUMN_COST,
            wineSQLiteHelper.COLUMN_RATING,
            wineSQLiteHelper.COLUMN_NOTES,
    };

    private Context context;
    private wineSQLiteHelper helper;

    public WineDataSource(Context context) {

        dbHelper = new wineSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Wine createWine(Wine wine) {
        ContentValues values = new ContentValues();
        values.put(wineSQLiteHelper.COLUMN_NAME, wine.getName());
        values.put(wineSQLiteHelper.COLUMN_BRAND, wine.getBrand());
        values.put(wineSQLiteHelper.COLUMN_ML, wine.getMl());
        values.put(wineSQLiteHelper.COLUMN_RETAILER, wine.getRetailer());
        values.put(wineSQLiteHelper.COLUMN_COST, wine.getCost());
        values.put(wineSQLiteHelper.COLUMN_RATING, wine.getRating());
        values.put(wineSQLiteHelper.COLUMN_NOTES, wine.getNotes());

        long insertId = database.insert(wineSQLiteHelper.TABLE_WINES, null,
                values);

        Cursor cursor = database.query(wineSQLiteHelper.TABLE_WINES,
                allColumns, wineSQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();

        Wine newWine = cursorToWine(cursor);

        cursor.close();

        return newWine;
    }

    public boolean updateWine(Wine wine) {
        ContentValues values = new ContentValues();
        values.put(wineSQLiteHelper.COLUMN_NAME, wine.getName());
        values.put(wineSQLiteHelper.COLUMN_BRAND, wine.getBrand());
        values.put(wineSQLiteHelper.COLUMN_ML, wine.getMl());
        values.put(wineSQLiteHelper.COLUMN_RETAILER, wine.getRetailer());
        values.put(wineSQLiteHelper.COLUMN_COST, wine.getCost());
        values.put(wineSQLiteHelper.COLUMN_RATING, wine.getRating());
        values.put(wineSQLiteHelper.COLUMN_NOTES, wine.getNotes());

        int rowsUpdated = database.update(wineSQLiteHelper.TABLE_WINES, values, "_id=?", new String[]{String.valueOf(wine.getId())});

        Cursor cursor = database.query(wineSQLiteHelper.TABLE_WINES,
                allColumns, wineSQLiteHelper.COLUMN_ID + " = " + wine.getId(), null,
                null, null, null);
        cursor.moveToFirst();
        cursor.close();

        return rowsUpdated > 0;
    }

    public void deleteWine(Wine wine) {
        long id = wine.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(wineSQLiteHelper.TABLE_WINES, wineSQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Wine> getAllWines() {
        List<Wine> wines = new ArrayList<Wine>();

        Cursor cursor = database.query(wineSQLiteHelper.TABLE_WINES,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Wine wine = cursorToWine(cursor);
            wines.add(wine);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return wines;
    }

    private Wine cursorToWine(Cursor cursor) {
        Wine wine = new Wine();
        wine.setId(cursor.getLong(0));
        wine.setName(cursor.getString(1));
        wine.setBrand(cursor.getString(2));
        wine.setRetailer(cursor.getString(3));
        wine.setMl(Double.parseDouble(cursor.getString(4)));
        wine.setCost(Double.parseDouble(cursor.getString(5)));
        wine.setRating(Float.parseFloat(cursor.getString(6)));
        wine.setNotes(cursor.getString(7));
        return wine;
    }
}
