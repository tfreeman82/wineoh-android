package com.example.tristanfreeman.winehound;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tristanfreeman on 3/4/15.
 */
public class wineAdapter extends ArrayAdapter<Wine> {

    // declaring our ArrayList of items
    private List<Wine> objects;

    /* here we must override the constructor for ArrayAdapter
    * the only variable we care about now is ArrayList<Item> objects,
    * because it is the list of objects we want to display.
    */
    public wineAdapter(Context context, int textViewResourceId, List<Wine> objects) {
        super(context, textViewResourceId, objects);
        this.objects = objects;
    }

    /*
     * we are overriding the getView method here - this is what defines how each
     * list item will look.
     */
    public View getView(int position, View convertView, ViewGroup parent){

        // assign the view we are converting to a local variable
        View v = convertView;

        // first check to see if the view is null. if so, we have to inflate it.
        // to inflate it basically means to render, or show, the view.
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.wine_list_cell, null);
        }

		/*
		 * Recall that the variable position is sent in as an argument to this method.
		 * The variable simply refers to the position of the current object in the list. (The ArrayAdapter
		 * iterates through the list we sent it)
		 *
		 * Therefore, i refers to the current Item object.
		 */
        Wine wine = objects.get(position);

        if (wine != null) {

            // This is how you obtain a reference to the TextViews.
            // These TextViews are created in the XML files we defined.

            TextView wineNameView = (TextView)v.findViewById(R.id.wineNameView);
            RatingBar wineRatingBar = (RatingBar)v.findViewById(R.id.rating);

            // check to see if each individual textview is null.
            // if not, assign some text!
            if (wineNameView != null){
                wineNameView.setText(wine.getName());
            }
            if (wineRatingBar != null) {
                wineRatingBar.setRating(wine.getRating());
            }
        }

        // the view must be returned to our activity
        return v;

    }
}
