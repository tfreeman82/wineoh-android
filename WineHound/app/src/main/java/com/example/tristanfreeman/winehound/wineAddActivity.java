package com.example.tristanfreeman.winehound;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;

import java.util.Random;


public class wineAddActivity extends ActionBarActivity {
    public static String addOrUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine_add);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new wineAddFragment())
                    .commit();
            ActionBar bar = getSupportActionBar();
            bar.setBackgroundDrawable(new ColorDrawable(0xFFC14276));
            bar.setTitle("Save A Wine");
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class wineAddFragment extends Fragment {
        RatingBar wineRating;
        EditText wineName;
        EditText wineBrand;
        EditText wineMl;
        EditText wineRetailer;
        EditText wineCost;
        EditText wineNotes;

        private Wine wine;

        public wineAddFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            inflater.inflate(R.menu.menu_wine_add, menu);
        }



        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_wine_add, container, false);

            Intent intent = getActivity().getIntent();

            wineRating = (RatingBar)rootView.findViewById(R.id.rating);
            wineName = (EditText)rootView.findViewById(R.id.wineName);
            wineBrand = (EditText)rootView.findViewById(R.id.wineBrand);
            wineMl = (EditText)rootView.findViewById(R.id.wineMl);
            wineRetailer = (EditText)rootView.findViewById(R.id.wineRetailer);
            wineCost = (EditText)rootView.findViewById(R.id.wineCost);
            wineNotes = (EditText)rootView.findViewById(R.id.wineNotes);

            if(intent != null){
                addOrUpdate = intent.getStringExtra("method");

                if (addOrUpdate.equals("add")) {
                    wine = new Wine();
                } else if (addOrUpdate.equals("update")) {
                    wine = (Wine) intent.getSerializableExtra("wine");
                    wineName.setText(wine.getName());
                    wineBrand.setText(wine.getBrand());
                    wineRetailer.setText(wine.getRetailer());
                    wineMl.setText(String.valueOf(wine.getMl()));
                    wineCost.setText(String.valueOf(wine.getCost()));
                    wineRating.setRating(wine.getRating());
                    wineNotes.setText(wine.getNotes());
                }
            }
            return rootView;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            if(item.getItemId() == R.id.wineSave){
                wine.setName(wineName.getText().toString());
                wine.setBrand(wineBrand.getText().toString());
                wine.setMl(Double.valueOf(wineMl.getText().toString()));
                wine.setRetailer(wineRetailer.getText().toString());
                wine.setCost(Double.valueOf(wineCost.getText().toString()));
                wine.setNotes(wineNotes.getText().toString());
                wine.setRating(Float.valueOf(wineRating.getRating()));

                if(addOrUpdate.equals("update")){
                    Intent updatedIntent = new Intent();
                    updatedIntent.putExtra("updatedWine", wine);
                    getActivity().setResult(Activity.RESULT_OK, updatedIntent);
                    getActivity().finish();
                }else{
                    Intent addedIntent = new Intent();
                    addedIntent.putExtra("addedWine", wine);
                    getActivity().setResult(Activity.RESULT_OK, addedIntent);
                    getActivity().finish();
                }

            }
            return super.onOptionsItemSelected(item);
        }
    }


}
