package com.example.tristanfreeman.winehound;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Random;

/**
 * Created by tristanfreeman on 4/3/15.
 */
public class Wine implements Serializable {

    private long id;
    private String name;
    private String brand;
    private String retailer;
    private double ml;
    private double cost;
    private String notes;
    private float rating;

    public Wine(){

    }

    public Wine(String name, String brand, double ml, String retailer, double cost, String notes, float rating){
        this.name = name;
        this.brand = brand;
        this.ml = ml;
        this.retailer = retailer;
        this.cost = cost;
        this.notes = notes;
        this.rating = rating;

    }

    public long getId(){
        return id;
    }
    public void setId(long id){
        this.id = id;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getBrand(){
        return brand;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }

    public double getMl(){
        return ml;
    }
    public void setMl(double ml){
        this.ml = ml;
    }

    public String getRetailer(){
        return retailer;
    }
    public void setRetailer(String retailer){
        this.retailer = retailer;
    }
    public double getCost(){
        return cost;
    }
    public void setCost(double cost){
        this.cost = cost;
    }

    public String getNotes(){
        return notes;
    }
    public void setNotes(String notes){
        this.notes = notes;
    }

    public float getRating(){
        return rating;
    }
    public void setRating(float rating){
        this.rating = rating;
    }


}
