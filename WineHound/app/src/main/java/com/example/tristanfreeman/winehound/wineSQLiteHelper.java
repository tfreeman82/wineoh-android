package com.example.tristanfreeman.winehound;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.net.ContentHandler;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tristanfreeman on 3/18/15.
 */
public class wineSQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_WINES = "Wine";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_RETAILER = "retailer";
    public static final String COLUMN_ML = "ml";
    public static final String COLUMN_COST = "cost";
    public static final String COLUMN_RATING = "rating";
    public static final String COLUMN_NOTES = "notes";

    private static final String DATABASE_NAME = "wine.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE =
            "create table " + TABLE_WINES
                    + "(" + COLUMN_ID + " integer primary key autoincrement, "
                    + COLUMN_NAME + " text not null, "
                    + COLUMN_BRAND + " text not null, "
                    + COLUMN_RETAILER + " text not null, "
                    + COLUMN_ML + " double not null, "
                    + COLUMN_COST + " double not null, "
                    + COLUMN_RATING + " float not null, "
                    + COLUMN_NOTES + " text not null"
                    + ");";

    public wineSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(wineSQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion
                + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WINES);
        onCreate(db);
    }
}