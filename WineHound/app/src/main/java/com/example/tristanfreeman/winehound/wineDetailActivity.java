package com.example.tristanfreeman.winehound;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;


public class wineDetailActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine_detail);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new WineDetailFragment())
                    .commit();

            ActionBar bar = getSupportActionBar();
            bar.setBackgroundDrawable(new ColorDrawable(0xFFC14276));
            bar.setTitle("Wine Details");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class WineDetailFragment extends Fragment {
        TextView nameLabel;
        TextView detailName;
        TextView brandLabel;
        TextView detailBrand;
        TextView mlLabel;
        TextView detailMl;
        TextView retailerLabel;
        TextView detailRetailer;
        TextView costLabel;
        TextView detailCost;
        TextView notesLabel;
        TextView detailNotes;
        RatingBar detailRating;

        private Wine wine;
        private boolean wasUpdated = false;

        public WineDetailFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            inflater.inflate(R.menu.menu_wine_detail, menu);
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_wine_detail, container, false);

            detailRating = (RatingBar)rootView.findViewById(R.id.detailRating);
            detailRating.setIsIndicator(true);
            nameLabel = (TextView)rootView.findViewById(R.id.detailNameLabel);
            detailName = (TextView)rootView.findViewById(R.id.detailName);
            brandLabel = (TextView)rootView.findViewById(R.id.detailBrandLabel);
            detailBrand = (TextView)rootView.findViewById(R.id.detailBrand);
            mlLabel = (TextView)rootView.findViewById(R.id.detailMlLabel);
            detailMl = (TextView)rootView.findViewById(R.id.detailMl);
            retailerLabel = (TextView)rootView.findViewById(R.id.detailRetailerLabel);
            detailRetailer = (TextView)rootView.findViewById(R.id.detailRetailer);
            costLabel = (TextView)rootView.findViewById(R.id.detailCostLabel);
            detailCost = (TextView)rootView.findViewById(R.id.detailCost);
            notesLabel = (TextView)rootView.findViewById(R.id.detailNotesLabel);
            detailNotes = (TextView)rootView.findViewById(R.id.detailNotes);

            Intent intent = getActivity().getIntent();
            if (intent != null) {
                wine = (Wine) intent.getSerializableExtra("wine");

                updateDisplayValues(wine);
            }

            return rootView;
        }

        public void updateDisplayValues(Wine w) {
            detailName.setText(w.getName());
            detailBrand.setText(w.getBrand());
            detailMl.setText(String.valueOf(w.getMl()) + "ml");
            detailRetailer.setText(w.getRetailer());
            detailCost.setText("$" + String.valueOf(w.getCost()));
            detailNotes.setText(w.getNotes());
            detailRating.setRating(w.getRating());
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            if(item.getItemId() == R.id.wineEdit){
                Intent editIntent = new Intent(getActivity(), wineAddActivity.class);
                editIntent.putExtra("wine", wine);
                editIntent.putExtra("method", "update");
                startActivityForResult(editIntent, 1);
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra("updatedWine")) {
                    wasUpdated = true;
                    wine = (Wine)data.getSerializableExtra("updatedWine");
                    Log.i("UPDATED WINE IN DETAIL", wine.toString());
                    updateDisplayValues(wine);
                }

                if (wasUpdated) {
                    Intent updatedIntent = new Intent();
                    updatedIntent.putExtra("updatedWine", wine);
                    getActivity().setResult(Activity.RESULT_OK, updatedIntent);
                }
                getActivity().finish();
            }
        }
    }
}
