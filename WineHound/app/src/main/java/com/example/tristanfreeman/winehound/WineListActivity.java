package com.example.tristanfreeman.winehound;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.quentindommerc.superlistview.SuperListview;
import com.quentindommerc.superlistview.SwipeDismissListViewTouchListener;

import java.util.ArrayList;
import java.util.List;


public class WineListActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine_list);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new wineListFragment())
                    .commit();
        }

        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(0xFFC14276));
        setTitle("Wine Tracker");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class wineListFragment extends Fragment {
        private WineDataSource datasource;
        private wineAdapter adapter;
        private Context mContext;
        private SuperListview wineList;

        public static wineListFragment newInstance(){
            wineListFragment fragment = new wineListFragment();
            return fragment;
        }

        public wineListFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            inflater.inflate(R.menu.menu_wine_list, menu);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            if(item.getItemId() == R.id.wineAdd){
                Intent addWineIntent = new Intent(getActivity(), wineAddActivity.class);
                addWineIntent.putExtra("method", "add");
                startActivityForResult(addWineIntent, 1);

                return true;
            }
            return super.onOptionsItemSelected(item);

        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            datasource.open();

            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra("addedWine")) {
                    Wine addedWine = datasource.createWine((Wine)data.getSerializableExtra("addedWine"));
                    Log.i("ADDED WINE", addedWine.toString());
                    adapter.add(addedWine);
                    adapter.notifyDataSetChanged();
                } else if (data.hasExtra("updatedWine")) {
                    boolean updatedWine = datasource.updateWine((Wine)data.getSerializableExtra("updatedWine"));
                    Log.i("UPDATED WINE", String.valueOf(updatedWine));
                    List<Wine> allWines = datasource.getAllWines();
                    adapter = new wineAdapter(mContext, R.layout.wine_list_cell, allWines);
                    wineList.setAdapter(adapter);
                }
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_wine_list, container, false);

            mContext = getActivity();

            wineList = (SuperListview)rootView.findViewById(R.id.wineList);

            datasource = new WineDataSource(mContext);
            datasource.open();

            List<Wine> allWines = datasource.getAllWines();
            adapter = new wineAdapter(mContext, R.layout.wine_list_cell, allWines);
            wineList.setAdapter(adapter);

            wineList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Wine selectedWine = (Wine) adapter.getItem(position);
                    Intent detailIntent = new Intent(getActivity(), wineDetailActivity.class);
                    detailIntent.putExtra("wine", selectedWine);
                    startActivityForResult(detailIntent, 1);

                }
            });

            //setup the swipe to delete for SuperListview
            wineList.setupSwipeToDismiss(new SwipeDismissListViewTouchListener.DismissCallbacks() {
                @Override
                public boolean canDismiss(int position) {
                    return true;
                }

                @Override
                public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                    for (int position : reverseSortedPositions) {
                        final Wine deleteWine = (Wine) adapter.getItem(position);
                        if (deleteWine != null) {
                            //alert dialog to confirm that the user wants to delete that entry.
                            new AlertDialog.Builder(getActivity())
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Delete Wine")
                                    .setMessage("Are you sure you want to delete this wine?")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Wine wineToDelete = deleteWine;
                                            adapter.remove(wineToDelete);
                                            adapter.notifyDataSetChanged();
                                        }
                                    })
                                    .setNegativeButton("No", null).show();

                        } else {
                            Log.i("DELETE", "ERROR");
                        }

                    }

                }
            }, false);

            return rootView;
        }

        @Override
        public void onResume() {
            datasource.open();
            super.onResume();
        }

        @Override
        public void onPause() {
            datasource.close();
            super.onPause();
        }

    }
}
